import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import MainContent from './components/app/MainContent';

import './index.css';

ReactDOM.render(
    <BrowserRouter>
        <MainContent />
    </BrowserRouter>,
    document.getElementById('root'),
);
