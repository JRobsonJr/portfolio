import React from 'react';
import { Switch, Route } from 'react-router-dom';

import HomePage from '../home/HomePage';
import SoberPage from '../sober/SoberPage';
import FriendlyPage from '../friendly/FriendlyPage';

const MainContent = () => (
    <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/sober" component={SoberPage} />
        <Route exact path="/friendly" component={FriendlyPage} />
    </Switch>
);

export default MainContent;
