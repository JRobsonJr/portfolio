import React from 'react';

const HomePage = () => (
    <div className="jumbotron">
        <JumbotronTop />
        <hr className="my-4" />
        <JumbotronBottom />
    </div>
);

const JumbotronTop = () => (
    <div>
        <h1 className="display-2">Hey there!</h1>
        <p className="lead">
            My name is Robson, and it is a pleasure to meet you!
        </p>
    </div>
);

const JumbotronBottom = () => (
    <div>
        <p>
            Moreover, I am a 19-year-old Brazilian student. Well, since you are
            reading my portfolio, you probably want to learn more than my name
            or my age.
        </p>
        <p>
            When I was thinking of what I should display in this page, I
            realized an important fact — I have no idea of who you are nor what
            you are up to. Because of that, I decided to give you the
            opportunity to choose whatever fits your profile the best: a
            straightforward text or a wordier and warmer introduction to me.
            That is up to you to decide!
            <i>
                (You could also check both if you have the time. Just saying.)
            </i>
        </p>
    </div>
);

export default HomePage;
